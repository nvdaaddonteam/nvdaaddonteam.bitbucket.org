var load;
(function(){
	var oldSelection;
	"use strict"; //strict mode is good.
	function done(responseText){
		var htmlPage = document.getElementById("page");
		marked(responseText, function (err, renderedHTML) {
			if (err) throw err;
			htmlPage.innerHTML = renderedHTML;
			var qs = htmlPage.querySelector("h1");
			document.title = qs.innerText;
			qs.tabIndex = -1;
			document.body.setAttribute("aria-busy", "false");
			qs.focus();
		});
	}
	var recursionSafety = 0; //If this gets above two, the error page is not tried again.
	load = function(page){ //export it from the  closure
		var item = document.getElementById("nav"+page);
		if(item){ //This is a menu item.
			oldSelection.style.color = "blue";
			oldSelection.setAttribute("aria-selected", "false");
			item.setAttribute("aria-selected", "true");
			item.style.color="red";
			oldSelection = item;
		}
		if(recursionSafety >1) {
			recursionSafety = 0;
			done("<h1>Error</h1>error, contact the NVDA Addon Team.");
		}
		document.body.setAttribute("aria-busy", "true");
		
		//based on w3 schools example
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				recursionSafety = 0;
				done(xhttp.responseText);
			}
			else
			{
				if(xhttp.status == 404){
					recursionSafety++;
					load("404.md")
				}
			}
		};
		xhttp.open("GET", page, true);
		xhttp.send();
	}
	function run(){
		//Url is the first side of the hash, the link text is the second item.
		var i=0;
		for(i in files){
			var navLiItem = document.createElement("li");
			var navItem = document.createElement("a");
			navItem.setAttribute("role", "tab");
			navItem.setAttribute("id", "nav"+i);
			navItem.setAttribute("href", "javascript:void(load('"+i+"'));");
			var node = document.createTextNode(files[i]+"\n");
			navItem.appendChild(node);
			navLiItem.appendChild(navItem);
			var element = document.getElementById("nav");
			element.appendChild(navLiItem);
		}
		var fileParam = document.URL.match(/(.*?)\?file\=(.+?)(&.+|$)/);
	oldSelection = document.getElementById("navindex.md")
		if(fileParam){
			var fileName = fileParam[1].match(/(.*)(\/|\?)/)[1]+"/"+fileParam[2];
			load(fileName);
		}
		else{
			load("index.md");
		}
			
	}
	window.addEventListener("load", run);
})(); //closure