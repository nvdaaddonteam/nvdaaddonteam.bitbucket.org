# Best Practices for Add-on development

This guide serves to explain the things that other add-on authors have done that they have found useful while developing add-ons. It also introduces you to tools NVDA provides that can make your add-on writing experience easier.
<!-- Skeleton -->
## Translation system

## Configuration

## Speech utilities.

## Layered keyboard commands

## When to and when not to monkey patch

## Extend this over time.
