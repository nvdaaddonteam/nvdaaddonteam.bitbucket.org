# Developer Documentation for NVDA
The development documentation for NVDA comprises two components.

1. The Developer Guide.
2. The api documentation, built from the source code itself.

## Developer guide.

The [developer guide (External site)](http://www.nvaccess.org/files/nvda/documentation/developerGuide.html) is the first thing you should read when learning about designing add-ons. This guide explains all the basic concepts for writing add-ons and more. Here is where you learn about global plugins, app modules, synthesizer and braille display drivers, and the NVDA python console and other tools helpful for add-on development.

## API Documentation

The [API Documentation (External site)](https://files.derekriemer.com/nvda) is built from the source code of NVDA, and documents all of the methods available for add-ons to use to achieve their goals. The documentation lists all the modules available. Inside each is a list of all classes and their docstrings. To use this, select the class you want, and find the method that achievs what functionality you need.
